package ru.tsc.tambovtsev.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.dto.model.TaskDTO;

@NoArgsConstructor
public final class TaskBindProjectResponse extends AbstractTaskResponse {

    public TaskBindProjectResponse(@Nullable TaskDTO task) {
        super(task);
    }

}
