package ru.tsc.tambovtsev.tm.api.service.property;

import org.jetbrains.annotations.NotNull;

public interface IServerPropertyService {

    @NotNull
    Integer getServerPort();

    @NotNull
    String getServerHost();

}
